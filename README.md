AspectMock test::double() problem demonstration
==========

A simple test project to reproduce a problem with AspectMocks test::double()
Prereqs:
* PHP 5.6.28
* Composer 1.2-dev

Use these commands:
```
composer install
vendor/bin/phpunit --configuration tests/phpunit-config.xml UnitTest tests/UserModelTest.php
```

Get this error:
```
There was 1 failure:

1) UserModelTest::testTableName
Failed asserting that two strings are equal.
--- Expected
+++ Actual
@@ @@
-'my_users'
+'users'
```
