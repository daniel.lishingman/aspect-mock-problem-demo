<?php

namespace demo_test;
use \AspectMock\Test as test;
use \demo\UserModel as UserModel;

/**
 * Class UserModelTest.
 */
class UserModelTest extends \PHPUnit_Framework_TestCase {


  /**
   * Perform setup.
   */
  public function setup() {
  }

  /**
   * Perform teardown.
   */
  public function teardown() {
    test::clean();
  }

  /**
   * @test
   */
  function testTableName()
  {
    $this->assertEquals('users', UserModel::tableName());
    $userModel = test::double('demo\UserModel', ['tableName' => 'my_users']);
    $this->assertEquals('my_users', UserModel::tableName());
    $userModel->verifyInvoked('tableName');
  }
}
